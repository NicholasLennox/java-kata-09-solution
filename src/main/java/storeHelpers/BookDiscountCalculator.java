package storeHelpers;

import java.util.*;

public class BookDiscountCalculator {
    // 0,1,2,3
    public double price(int[] booksPurchased) {
        if (booksPurchased.length == 0) {
            return 0;
        }
        if(booksPurchased.length == 1){
            return 8;
        }
        double[] discounts = {1,1,0.95,0.9,0.8};
        ArrayList<Integer> booksList = convertArrayToArrayList(booksPurchased);
        // List of unique books -> List of a list
        ArrayList<ArrayList<Integer>> uniquesList = new ArrayList<>();
        // Extract unique books -> uses recursion
        extractUniqueBooks(booksList, uniquesList);
        double totalPrice = 0;
        for (ArrayList<Integer> uniqueBooks: uniquesList) {
            totalPrice += 8*uniqueBooks.size()*discounts[uniqueBooks.size()];
        }
        return totalPrice;
    }

    private void extractUniqueBooks(ArrayList<Integer> booksList, ArrayList<ArrayList<Integer>> uniquesList){
        HashSet<Integer> uniqueBooks = new HashSet<>(booksList);
        if(booksList.size() == uniqueBooks.size()){
            uniquesList.add(booksList);
        } else {
            // Remove all uniques from list
            for (Integer unique: uniqueBooks) {
                booksList.remove(unique);
            }
            uniquesList.add(new ArrayList<>(uniqueBooks));
            extractUniqueBooks(booksList,uniquesList);
        }
    }

    private ArrayList<Integer> convertArrayToArrayList(int[] arr){
        ArrayList<Integer> retList = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            retList.add(arr[i]);
        }
        return retList;
    }

    //ArrayList<Integer> booksPurchasedArrayList = (ArrayList<Integer>) Arrays.stream(booksPurchased).boxed().collect(Collectors.toList());
    //ArrayList<Integer> uniqueBooksListStream = (ArrayList<Integer>) booksPurchasedArrayList.stream().distinct().collect(Collectors.toList());

}
