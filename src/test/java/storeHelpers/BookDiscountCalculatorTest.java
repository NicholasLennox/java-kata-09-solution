package storeHelpers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookDiscountCalculatorTest {

    BookDiscountCalculator store = new BookDiscountCalculator();

    @Test
    void testEmptyBookArray(){
        int[] books = {};
        assertEquals(0, store.price(books));
    }

    @Test
    void testOneUniqueBook(){
        int[] oneUniqueBook0 = {0};
        int[] oneUniqueBook1 = {1};
        int[] oneUniqueBook2 = {2};
        int[] oneUniqueBook3 = {3};
        assertEquals(8, store.price(oneUniqueBook0));
        assertEquals(8, store.price(oneUniqueBook1));
        assertEquals(8, store.price(oneUniqueBook2));
        assertEquals(8, store.price(oneUniqueBook3));
    }

    @Test
    void testMultipleDuplicateBook(){
        int[] duplicateBooks = {1,1,1};
        assertEquals(8*3, store.price(duplicateBooks));
    }

    @Test
    void testTwoUniqueBooks(){
        int[] books = {0,1};
        assertEquals(8 * 2 * 0.95, store.price(books));
    }

    @Test
    void testThreeUniqueBooks(){
        int[] books = {0, 2, 3};
        assertEquals(8*3*0.9, store.price(books));
    }

    @Test
    void testFourUniqueBooks(){
        int[] books = {0,1,2,3};
        assertEquals(8*4*0.8, store.price(books));
    }

    @Test
    void testMultipleDiscounts(){
        int[] books = {0,0,1};
        assertEquals(8 + (8*2*0.95),store.price(books));
    }

    @Test
    void testMultipleDiscountsExtra(){
        int[] books1 = {0,0,1,1};
        int[] books2 = {0,0,1,2,2,3};
        assertEquals(2*(8*2*0.95),store.price(books1));
        assertEquals((8*4*0.8)+(8*2*0.95), store.price(books2));
    }

}